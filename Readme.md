# Reddit Mirror videosite

## Notes

Check on this repository as it is an updated verion of this one Mirror-Site-Using-Reactjs

I uploaded the database loaded with some samples on it that link to imgur for the media. If you want to run it with
locally hosted media then just replace the link to point to your MEDIA folder for videos and thumbnails.

## Images Album

This is what the frontend looks like and the second image shows the message you get when there are no more videos to view
https://vidble.com/album/ULgbLSKS

## Installation

Install the requirements for the backend on django with 
```sh
pip install requirements.txt
```

## Mirror Site

For the frontend it uses the following technologies: 

- [Twitter Bootstrap] - great UI boilerplate for modern web apps
- [jQuery] - for communicating with the backend and implement infinite scroll

For the backend I utilized:

- [Django] - Backend Framework
- [Sqlite] - for storing and reading the records

## Installation Using Docker

Easiest way is to use the dockerfile and docker-compose file I included with the following command
```sh
docker compose build
docker compose run
docker compose up
```
This will create the mirror image and pull the necessary dependencies.
Once done, run the Docker image and map the port to whatever you wish on
your host. In this example, we simply map port 8000 of the host to
port 8080 of the Docker (or whatever port was exposed in the Dockerfile):

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:8000
```



[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
   [Django]: <https://docs.djangoproject.com/>
   [SQLite]: <https://www.sqlite.org/index.html>
   [Twitter Bootstrap]: <https://getbootstrap.com/>
   [jQuery]: <https://jquery.com>

