FROM python:3.8-slim-buster

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

WORKDIR /videosite

# Updating packages and installing cron
RUN apt-get update

# Install pip requirements
COPY requirements.txt .
#RUN python -m pip install -r requirements.txt
RUN python -m pip install -r requirements.txt

COPY . .