import datetime
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages

from .models import Posts
from .forms import getvideo
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views.decorators.cache import cache_page

# Create your views here.

def index(request):
    
    print(datetime.datetime.fromtimestamp(13374532639))
    mycontent = { 'mycontent': 1277722499 
         
        }    
    return render(request, 'videos/home.html', mycontent
    )
    
@cache_page(60 * 10)
def watch_video(request, video_id):
    
    try:
        video_obj = Posts.objects.get(video_id=video_id)
        print(video_obj.title)
        
        return render(request, 'videos/videos.html', {
            'videos': video_obj
        } )
    except ObjectDoesNotExist:
        return render(request, 'videos/home.html', {
            'error', 'error'
        })

def load_videos(request):
    
    if request.method == 'GET':
        page_number = request.GET.get('page', None)
        post_list = Posts.objects.order_by('-timesamp_in_utc')
        paginator = Paginator(post_list, 4) # 
        try:
            queryset = paginator.page(page_number)
        except PageNotAnInteger:
            print('Page number is not an integer it will still return the results for page 1')
            
            queryset = paginator.page(1)
        except EmptyPage:
            print('Page value is empty')
            return HttpResponse(request, '')
    
        return render(request, 'videos/load_videos.html', {'videos': queryset})
    
