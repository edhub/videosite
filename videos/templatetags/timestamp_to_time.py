import datetime
from django import template    
register = template.Library()    


#https://stackoverflow.com/questions/10715253/display-timestamp-in-django-template
#https://www.appsloveworld.com/django/100/30/how-do-i-convert-unix-timestamp-in-integer-to-human-readable-format-in-django-tem
@register.filter('timestamp_to_time')
def convert_timestamp_to_time(timestamp):
    import time
    return datetime.date.fromtimestamp(int(timestamp))