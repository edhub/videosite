from django.db import models

# Create your models here.

class Posts(models.Model):
    permalink= models.CharField(max_length=100)
    title= models.CharField(max_length=100)
    #reddit_post_id= models.CharField(max_length=10)
    video_id= models.CharField(max_length=25, unique=True)
    video_thumbnail= models.CharField(max_length=100)
    timesamp_in_utc= models.IntegerField(null=True)
    
    
    #this is used to tell the model class to order the posts in descending order as in newest to oldest
    class Meta:
        ordering = ['-timesamp_in_utc']
        
    def __str__(self):
        return self.title
    

#for creating a drf api accepting timestamps and converting them to datetime    
'''
class TravelSerializer(serializers.ModelSerializer):
    departure_at = serializers.IntegerField(write_only=True)
    arrival_at = serializers.IntegerField(write_only=True)

    def validate_departure_at(self, value):
        return datetime.fromtimestamp(value)

    def validate_arrival_at(self, value):
        return datetime.fromtimestamp(value)

    class Meta:
        model = Travel
        fields = ('departure_at', 'arrival_at')
'''    
"""
class Videos(models.Model):
    video_id = models.ForeignKey(Posts, on_delete=models.CASCADE)
"""
