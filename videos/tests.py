from django.test import TestCase, SimpleTestCase
from django.urls import reverse, resolve  

from videos.models import Posts
# Create your tests here.

class PostsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        print('setUpClass method called!!')     
        # Create 13 authors for pagination tests
        # number_of_posts = 13

        # for post_id in range(number_of_posts):
        #     Posts.objects.create(
        #         first_name=f'Dominique {author_id}',
        #         last_name=f'Surname {author_id}',
        #     )   
            
        cls.post = Posts.objects.create(permalink='/r/flying/comments/10ju4zj/faa_drug_test_check_also_for_alcohol_or_is_it_a/', title='Faa drug test check also for alcohol or is it a different test?', video_id='10ju4zj', video_thumbnail='10ju4zj', timesamp_in_utc=1674525334)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass method called!!')
        cls.post.full_clean()
                                      
    def test_posts(self):

        permalink='/r/flying/comments/10ju4zj/faa_drug_test_check_also_for_alcohol_or_is_it_a/'
        title='Faa drug test check also for alcohol or is it a different test?'
        video_id='10ju4zj'
        video_thumbnail='10ju4zj'
        timesamp_in_utc=1674525334
        post = Posts.objects.get(id=1)
        print(post.title)
        self.assertEquals(str(post.permalink), permalink)
        self.assertEquals(str(post.title), title)
        self.assertEquals(str(post.video_id), video_id)
        self.assertEquals(str(post.video_thumbnail), video_thumbnail)
        self.assertEquals(post.timesamp_in_utc, timesamp_in_utc)

        print("IsInstance : ",isinstance(post,Posts))
        self.assertTrue(isinstance(post, Posts))
        

    def test_title_name(self):
        post = Posts.objects.get(id=1)
        expected_object_name = post.title
        self.assertEqual(str(post), expected_object_name)
        
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/post/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        
    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'videos/home.html')        

    def test_video_exists(self):
        post = Posts.objects.get(id=1)
        print(post.video_id)
        response = self.client.get('/post/'+ str(post.video_id + "/"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'videos/videos.html')

    def test_pagination_is_ten(self):
        response = self.client.get(reverse('load_videos'))

    def test_HomePage(self):
        response = self.client.get(reverse('home'))
        self.assertContains(response, "Mirror Site")
        self.assertNotContains(response, "Not on the page")        
     
     
     
           