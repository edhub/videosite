from django.contrib import admin
from . models import Posts


# Register your models here.


class DisplayVideos(admin.ModelAdmin):
  list_display = ('title', 'permalink', 'video_id')
  list_filter = ('video_id', )
  #prepopulated_fields = {'slug': ('title', )}

admin.site.register(Posts, DisplayVideos)

