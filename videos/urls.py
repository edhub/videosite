from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('load/', views.load_videos, name='load_videos'),
    path('<str:video_id>/', views.watch_video, name='watch_video'),

]

